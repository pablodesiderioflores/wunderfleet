# wunderfleet
Challenge assignment

* The architecture I choose was clean architecture.
With clean architecture is easier to add unit test to the code. For example if you want to test view model or presenter, you can mock use cases and return the data needed to the mocked view. 
Also is easier to find errors because its have layers for the view, for bussines model and remote data, cache or in memory data well defined, separated and decouple. So is more mantainable, we can adapt to changes quickly.
On the view layer I choosed MVVM because in this way we don't have a reference of the view and we avoid some issues like with presenters for example. The viewmodel class from android has a different lifecycle from the activity or fragment, it surives on configuration changes for example, and with live data we can just observe data from the view and work in more reactive ways.
But, as "no silver bullets" book says, there is no yet "one fit for all solutions" so, this kind of thing dependen on the project and the bussines.

* Which things could be done better, than you’ve done it?

I think it can be improved data layer, because I created this layer as an android library, I thought I will be using room library for cache but I didin't have enough time for do it.
On domain layer I have a baseUseCase class, I thought I would extend a Completable class  from the baseUseCase but I didin't, I only used and ObservableUseCase. I could have also extend from disposable, in order to not have empty onCompletable callbacks. 
On the view layer, regarding to view model factories I created one for each view model instead of has just one and to use multibinding from dagger and inject anything necesary on view models.
Regarding to the table layout I thing it is a lot of work for rendering this view.
I wanted to add geofence in order to use vehicles in a zone a let the propietaries know when a vehicle is to far from one zone.
I could have use clustering too in irder to the user understand if there are a lot of vehicle in one place, beacuse now you only visualize a big marker if you don't have the view map zoomed.

* Describe possible performance optimizations for your Code.

Maybe the table layout rendering is to heavy for the view. Maybe it would be better to use a recyclerview  and draw some kind of table with this. 

* I have another pet projects using kotlin and coroutines for single shot operations like Completable, Single, Maybe and with coroutines flow like Observable/Flowable.

* Links: 

* https://github.com/PabloDesiderioFlores/kotlin-flow

* https://github.com/PabloDesiderioFlores/android-challenge

Some Libraries use it in this challenge.

* rxJava
* rxAndroid
* dagger
* daggerAndroid
* timber
* retrofit
* okHttp
* gson
* glide

* ViewModel
* LiveData
* databinding


Clean Architecture + MVVM

